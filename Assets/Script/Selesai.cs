﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selesai : MonoBehaviour
{
    public GameObject Notifaktif, CentangAktif;
    // Notification
    public void NotifAktif()
    {
        Notifaktif.SetActive(true);
    }
    public void CentangAktifs()
    {
        CentangAktif.SetActive(true);
    }
}
