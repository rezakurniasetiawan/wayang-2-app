﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameplay1 : MonoBehaviour
{
    public string[] soal, jawaban;


    public Text text_soal, text_skor, WarningText;

    public InputField input_jawaban;

    public GameObject feed_benar, feed_salah, selesai, banksoal, lanjut, ulangi;

    int urutan_soal = -1, skor = 0;
    private void Start()
    {
        tampil_soal();
    }

    void tampil_soal()
    {
        urutan_soal++;
        text_soal.text = soal[urutan_soal];
    }

    public void jawab()
    {
   
        if (string.IsNullOrEmpty(input_jawaban.text))
        {
            WarningText.text = "Input is empty";
            WarningText.gameObject.SetActive(true);
        }
        else
        {
            WarningText.gameObject.SetActive(false);
            if (urutan_soal < soal.Length - 1)
            {
                if (input_jawaban.text == jawaban[urutan_soal])
                {
                    skor += 20;
                }
                else
                {
                }
                input_jawaban.text = "";
                tampil_soal();
            }
            else
            {
                if (input_jawaban.text == jawaban[urutan_soal])
                {
                    skor += 20;
                }
                selesai.SetActive(true);
                banksoal.SetActive(false);
            }
        }
         
     

    }
    void Update()
    {
        text_skor.text = skor.ToString();

        if (skor >= 20 )
        {
            ulangi.SetActive(false);
            feed_benar.SetActive(true);
            feed_salah.SetActive(false);
            lanjut.SetActive(true);
        }
        else
        {
            lanjut.SetActive(false);
            feed_salah.SetActive(true);
            ulangi.SetActive(true);
        }

    }
    public void hapusjwb1()
    {
        input_jawaban.text = "";
    }
}
