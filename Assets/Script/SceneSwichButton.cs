﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwichButton : MonoBehaviour
{
    // Start is called before the first frame update
    public void LoadLevel1()
    {
        SceneManager.LoadScene("MainLevel01");
    }
    public void LoadLevel2()
    {
        SceneManager.LoadScene("MainLevel02");
    }
    public void LoadLevel3()
    {
        SceneManager.LoadScene("MainLevel03");
    }




    // Quid Game
    public void QuitGame()
    {
        PlayerPrefs.SetInt("LoadSaved", 8);
        PlayerPrefs.SetInt("SavedScene", SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene("MulaiGame");
    }
}
