﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour

{
    // Start is called before the first frame update
    public void scele(float scele)
    {
        transform.localScale = new Vector2(1 / scele, 1 * scele);
    }
    public void Mlebu()
    {
        SceneManager.LoadScene("MelbuScreen");
    }
    public void NgenaniApps()
    {
        SceneManager.LoadScene("NgenaniApps");
    }
    public void IndikatorScreen()
    {
        SceneManager.LoadScene("IndikatorScreen");
    }
    public void GladhenScreen()
    {
        SceneManager.LoadScene("GladhenScreen");
    }
    public void GladhenScreen2()
    {
        SceneManager.LoadScene("GladenScreen2");
    }
    public void DetileCerkak1()
    {
        SceneManager.LoadScene("DetileCerkak1");
    }
    public void DetileCerkak2()
    {
        SceneManager.LoadScene("DetileCerkak2");
    }

    public void TuladhaScreen()
    {
        SceneManager.LoadScene("TuladhaScreen");
    }
    public void MulaiGame()
    {
        SceneManager.LoadScene("MulaiGame");
    }

    // Cerkak
    public void KuisCerkak1()
    {
        SceneManager.LoadScene("KuisCerkak1");
    }
    public void KuisCerkak2()
    {
        SceneManager.LoadScene("KuisCerkak2");
    }
    public void KuisCerkak3()
    {
        SceneManager.LoadScene("KuisCerkak3");
    }
    public void KuisCerkak4()
    {
        SceneManager.LoadScene("KuisCerkak4");
    }
    public void KuisCerkak5()
    {
        SceneManager.LoadScene("KuisCerkak5");
    }
    public void KuisCerkak6()
    {
        SceneManager.LoadScene("KuisCerkak6");
    }
    public void KuisCerkak7()
    {
        SceneManager.LoadScene("KuisCerkak7");
    }
    public void KuisCerkak8()
    {
        SceneManager.LoadScene("KuisCerkak8");
    }
    //Kuis 
    public void Soal1()
    {
        SceneManager.LoadScene("Cerkak1Kuis1");
    }
    public void Soal2()
    {
        SceneManager.LoadScene("Cerkak1Kuis2");
    }
    public void Soal3()
    {
        SceneManager.LoadScene("Cerkak1Kuis3");
    }
    public void Soal4()
    {
        SceneManager.LoadScene("Cerkak1Kuis4");
    }
    public void Soal5()
    {
        SceneManager.LoadScene("Cerkak2Kuis5");
    }
    public void Soal6()
    {
        SceneManager.LoadScene("Cerkak2Kuis6");
    }
    public void Soal7()
    {
        SceneManager.LoadScene("Cerkak2Kuis7");
    }



    // Function Back All
    public void BackMain()
    {
        SceneManager.LoadScene("MainScreen");
    }
    public void BackMlebu()
    {
        SceneManager.LoadScene("MelbuScreen");
    }
    public void BackGladhenScreen()
    {
        SceneManager.LoadScene("GladhenScreen");
    }
    public void BackGladhenScreen2()
    {
        SceneManager.LoadScene("GladenScreen2");
    }
    public void BackDetileCerkak1()
    {
        SceneManager.LoadScene("DetileCerkak1");
    }


    
}
