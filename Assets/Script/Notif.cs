﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notif : MonoBehaviour
{

    public GameObject Notifaktif, NotifNonaktif;
    // Notification
    public void NotifAktif()
    {
        Notifaktif.SetActive(true);
    }
    public void NotifNonAktif()
    {
        NotifNonaktif.SetActive(false);
    }
}
